/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************************!*\
  !*** ./resources/js/users-activity-chart.js ***!
  \**********************************************/
var data = {
  labels: importLabels,
  datasets: [{
    label: 'Posts',
    data: importData,
    backgroundColor: 'rgb(255, 99, 132)',
    borderColor: 'rgb(255, 99, 132)'
  }]
};
var config = {
  type: 'bar',
  data: data,
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    responsive: true,
    plugins: {
      legend: {
        position: 'top'
      },
      title: {
        display: true,
        text: 'Top 10 Authors'
      }
    }
  }
};
var myChart = new Chart(document.getElementById('myChart'), config);
/******/ })()
;
# Zadanie Grupa RBR <img src="https://www.gruparbr.pl/wp-content/uploads/2020/11/cropped-logo_gruparbr_logo.png" alt="grupa rbr" width="120">

Proszę napisać aplikację webową w języku **PHP** (z użyciem frameworka Laravel) za pomocą cyklicznego skryptu (raz dziennie),
który będzie pobierać dane z API (linki są podane poniżej) użytkowników oraz ich postów i końcowo aktualizować je w bazie danych **MySQL**.

API do użycia w zadaniu rekrutacyjnym:
- Users: https://jsonplaceholder.typicode.com/users
- Posts: https://jsonplaceholder.typicode.com/posts

Listę treści postów wraz z przypisanym do niego autorem wyświetl w widoku w postaci tabeli.
Dodatkowo proszę przedstawić na wykresie najbardziej aktywnych użytkowników z ostatnich 7 dni.

Rozwiązane zadanie proszę wrzucić na zdalne repozytorium **git** i podać do niego link.
Rozwiązanie proszę odesłać na następujący adres e-mail rekrutacja@gruparbr.pl

Prosze wykonać zadanie w ciągu 4 dni od momentu dostarczenia wiadomości.

W razie pytań, proszę się zgłaszać do nas poprzez adres email:
[rekrutacja@gruparbr.pl](mailto:rekrutacja@gruparbr.pl).

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class StatisticController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = Post::select(DB::raw('count(*) as post_count, user_id'))
            ->with(['user:id,name'])
            ->where('ghosted', false)
            ->whereBetween('created_at', [Carbon::now()->sub('7 days'), Carbon::now()])
            ->groupBy('user_id')
            ->orderBy('post_count', 'desc')
            ->limit(10)
            ->get();
        $authors = $posts->pluck('user')->flatten()->pluck('name')->toArray();
        $countsOfPosts = $posts->pluck('post_count')->toArray();
        return view('statistic.index', [
            'authors' => $authors,
            'countsOfPosts' => $countsOfPosts
        ]);
    }
}

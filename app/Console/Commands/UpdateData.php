<?php

namespace App\Console\Commands;

use App\Models\Address;
use App\Models\Company;
use App\Models\Post;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->updateUsers(Http::get(config('services.jsonplaceholder.domain') . '/users'));
        $this->updatePosts(Http::get(config('services.jsonplaceholder.domain') . '/posts'));

        return 0;
    }

    /**
     * @param $usersResponse
     */
    private function updateUsers($usersResponse): void
    {
        $basicInformation = [];
        $addresses = [];
        $companies = [];
        if ($usersResponse->ok()) {
            $usersResponse->collect()
                ->each(function ($item, $key) use (&$basicInformation, &$addresses, &$companies) {
                    $basicInformation[] = [
                        'id' => $item['id'],
                        'name' => $item['name'],
                        'username' => $item['username'],
                        'email' => $item['email'],
                        'phone' => $item['phone'],
                        'website' => $item['website'],
                        'ghosted' => false,
                    ];
                    $addresses[] = [
                        'user_id' => $item['id'],
                        'street' => $item['address']['street'],
                        'suite' => $item['address']['suite'],
                        'city' => $item['address']['city'],
                        'zip_code' => $item['address']['zipcode'],
                        'geo_lat' => $item['address']['geo']['lat'],
                        'geo_lng' => $item['address']['geo']['lng']
                    ];
                    $companies[] = [
                        'user_id' => $item['id'],
                        'name' => $item['company']['name'],
                        'catch_phrase' => $item['company']['catchPhrase'],
                        'bs' => $item['company']['bs']
                    ];
                });

            User::upsert($basicInformation, ['id'], ['name', 'username', 'email', 'phone', 'website', 'ghosted']);
            Address::upsert($addresses, ['user_id'], ['street', 'suite', 'city', 'zip_code', 'geo_lat', 'geo_lng']);
            Company::upsert($companies, ['user_id'], ['name', 'catch_phrase', 'bs']);

            $deletedUsers = User::whereNotIn('id', $usersResponse->collect()->pluck('id')->toArray());
            $deletedUsers->update(['ghosted' => true]);
            $deletedUsers->get()
                ->each(function ($deletedUser, $key) {
                    $deletedUser->posts()->update(['ghosted' => true]);
                });
        }
    }

    private function updatePosts($postsResponse)
    {
        if ($postsResponse->ok()) {
            $postsResponse->collect()
                ->each(function ($item, $key) {
                    $post = Post::find($item['id']);
                    $user = User::find($item['userId']);

                    if ($post) {
                        if ($post->title !== $item['title'] || $post->body !== $item['body'] || $post->ghosted) {
                            $post->title = $item['title'];
                            $post->body = $item['body'];
                            $post->ghosted = false;
                            $post->save();
                        }
                    } else {
                        $post = new Post([
                            'id' => $item['id'],
                            'title' => $item['title'],
                            'body' => $item['body']
                        ]);
                        $user?->posts()->save($post);
                    }
                });

            Post::whereNotIn('id', $postsResponse->collect()->pluck('id')->toArray())->update(['ghosted' => true]);
        }
    }
}

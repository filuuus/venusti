<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property string $website
 * @property boolean $ghosted
 * @property Carbon $created_at
 * @property Carbon $update_at
 */
class User extends Authenticatable
{

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'phone',
        'website',
        'ghosted'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'ghosted' => 'boolean',
    ];

    /**
     * @return HasOne
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    /**
     * @return HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }

    /**
     * @return HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}

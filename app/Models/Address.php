<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $user_id
 * @property string $street
 * @property string $suite
 * @property string $city
 * @property string $zip_code
 * @property string $geo_lat
 * @property string $geo_lng
 * @property Carbon $created_at
 * @property Carbon $update_at
 */
class Address extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'street',
        'suite',
        'city',
        'zip_code',
        'geo_lat',
        'geo_lng'
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">
    @yield('style')
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item"><a href="/" class="nav-link {{ Request::path() === '/' ? 'active' : '' }}">Home</a></li>
                <li class="nav-item"><a href="/posts" class="nav-link {{ Request::is('posts*') ? 'active' : '' }}">Posts</a></li>
                <li class="nav-item"><a href="/statistics" class="nav-link {{ Request::path() === '/statistics' ? 'active' : '' }}">Statistics</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container pt-5">
    <div class="row">
        <h3 class="text-center pb-5">
            @yield('title')
        </h3>
    </div>
    @yield('content')
</div>
<script src="{{ mix('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>

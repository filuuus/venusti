@extends('layouts.app')

@section('title')
    Top 10 Authors
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <canvas id="myChart"></canvas>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        const importLabels = @json($authors);
        const importData = @json($countsOfPosts);
    </script>
    <script src="{{ mix('/js/users-activity-chart.js') }}"></script>
@endpush
